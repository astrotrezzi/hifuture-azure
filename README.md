# Certificazione Azure Data Scientist Associate

Il Data Scientist Azure applica le proprie conoscenze di data science e di machine learning per implementare ed eseguire carichi di lavoro di machine learning su Azure; in particolare, utilizzando Azure Machine Learning Service. Ciò comporta la pianificazione e la creazione di un ambiente di lavoro adatto per i carichi di lavoro di data science su Azure, l'esecuzione di esperimenti sui dati e la formazione sui modelli predittivi, la gestione e l'ottimizzazione dei modelli e l'implementazione di modelli di machine learning in produzione.
In questo esame verranno valutate le seguenti competenze:
- Impostare uno spazio di lavoro Azure Machine Learning (30-35%)
- Eseguire esperimenti e addestrare modelli (25-30%)
- Ottimizzare e gestire i modelli (20-25%)
- Distribuire e consumare i modelli (20-25%)

Per maggiori informazioni sull'esame DP-100 visitate il sito https://docs.microsoft.com/it-it/learn/certifications/exams/dp-100
Per maggiori informazioni sulla certificazione Azure Data Scientist Associate visitate il sito https://docs.microsoft.com/it-it/learn/certifications/azure-data-scientist/ .
